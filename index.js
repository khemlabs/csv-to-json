// Create and configure server

const filePath = './data/data.csv';
const match = require('./data/match.json');
const parser = require('./lib/parser');

console.log('Configuring parser...');
const parse = new parser(filePath, match.REQUIRED_COLUMNS, match.COLUMN_MATCHING, match.PARENT_ENTITY);

console.log('Init parsing...');
parse
  .start()
  .then(json => console.log('Parsing finished'))
  .catch(err => console.error(err));
