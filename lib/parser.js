const csv = require('csv'),
  async = require('async'),
  fs = require('fs'),
  path = require('path');

/**
 * Parser object
 * @param {string} FILE_PATH 
 * @param {array} REQUIRED_COLUMNS 
 * @param {array} COLUMN_MATCHING 
 * @param {string} PARENT_ENTITY 
 */
const parser = function(FILE_PATH, REQUIRED_COLUMNS, COLUMN_MATCHING, PARENT_ENTITY) {
  this.REQUIRED_COLUMNS = REQUIRED_COLUMNS || false;
  this.COLUMN_MATCHING = COLUMN_MATCHING || false;
  this.PARENT_ENTITY = PARENT_ENTITY || false;
  this.FILE_PATH = FILE_PATH || false;

  /**
	* Import data from file
	* @param {bin} csvData
	* @param {object} req
	*/
  this.start = (csvData, req) => {
    let data = [];
    return this.readFile()
      .then(fileData => this.parseCSV(fileData))
      .then(csvData => {
        if (csvData.length < 2) return Promise.reject('empty file');
        data = csvData;
        // Import data in order a1nd call callback
        console.log('Validating columns...');
        return this.checkColumns(data[0]);
      })
      .then(() => {
        if (!this.PARENT_ENTITY) return Promise.reject('PARENT_ENTITY is not set');
        if (!this.COLUMN_MATCHING && !Array.isArray(this.COLUMN_MATCHING)) {
          return Promise.reject('COLUMN_MATCHING is not a valid array');
        }
        const entity = this.PARENT_ENTITY
          ? this.COLUMN_MATCHING.find(data => data.entity == this.PARENT_ENTITY)
          : false;
        if (!entity) {
          return Promise.reject(`Parent entity ${this.PARENT_ENTITY} not found on COLUMN_MATCHING`);
        }
        let json = {};
        try {
          console.log(`Parsing ${data.length - 1} lines`);
          json[this.PARENT_ENTITY] = this.arrayWalk(data, this.getColumnsIndex(data.shift()), this.PARENT_ENTITY);
        } catch (err) {
          return Promise.reject(err);
        }
        console.log('Writing to file ./data/result.json');
        fs.writeFile(
          './data/result.json',
          JSON.stringify(json),
          err => (err ? Promise.reject(err) : Promise.resolve())
        );
      });
  };

  this.arrayWalk = (data, columnsIdxs, entityname, parentKeys = []) => {
    let elements = [];
    let indexes = {};
    // Find entity in COLUMN_MATCHING array
    const entity = this.COLUMN_MATCHING.find(data => data.entity == entityname);
    if (!entity) throw new Error(`Entity ${entity} not found on COLUMN_MATCHING`);
    // Find primary key of entity (this will be used to iterate)
    const primaryKey = entity.columns.find(col => col.primary);
    if (!primaryKey) throw new Error(`Primary key for entity ${primaryKey} not found on COLUMN_MATCHING`);
    data.forEach(element => {
      if (typeof columnsIdxs[primaryKey.column] != 'number') {
        throw new Error(`Column ${primaryKey.column} not found`);
      }
      // Check if is first time or if it has to check a parent entity
      const primaryText = element[columnsIdxs[primaryKey.column]] ? element[columnsIdxs[primaryKey.column]].trim() : '';
      // Check if has to be assigned to a parent
      if (parentKeys.length) {
        // It not same as parent
        const isNotChild = parentKeys.find(col => {
          if (typeof columnsIdxs[col.key] != 'number') {
            throw new Error(`Column ${col.key} not found`);
          }
          return element[columnsIdxs[col.key]] != col.value;
        });
        // Is not child early return
        if (isNotChild) return;
      }
      // It is the first time
      if (!indexes[entityname]) indexes[entityname] = [];
      if (primaryText && indexes[entityname].indexOf(primaryText) == -1) {
        indexes[entityname].push(primaryText);
        let insert = {};
        // Build element
        entity.columns.forEach(col => {
          if (typeof columnsIdxs[col.column] != 'number') {
            throw new Error(`Column ${col.column} not found`);
          }
          insert[col.jsonKey || col.column] = element[columnsIdxs[col.column]].trim();
        });
        // If entity has childs
        if (entity.child) {
          const child = this.COLUMN_MATCHING.find(data => data.entity == entity.child);
          if (!child) throw new Error(`Child ${entity.child} not found on COLUMN_MATCHING`);
          parentKeys.push({
            key: primaryKey.column,
            value: primaryText
          });
          insert[entity.child] = this.arrayWalk(data, columnsIdxs, entity.child, parentKeys);
          parentKeys.pop();
        }
        elements.push(insert);
      }
    });
    return elements;
  };

  /**
* Checks if all required columns exists
* @param {array} columns
*/
  this.checkColumns = columns => {
    return new Promise((resolve, reject) => {
      let colError = { columnsNotFound: [] };
      if (!this.REQUIRED_COLUMNS && !Array.isArray(this.REQUIRED_COLUMNS)) {
        return reject('REQUIRED_COLUMNS is not a valid array');
      }
      async.each(
        this.REQUIRED_COLUMNS,
        (column, next) => {
          if (columns.indexOf(column) >= 0) return next();
          colError.columnsNotFound.push(column);
          return next('not_found');
        },
        err => (err ? reject(JSON.stringify(colError)) : resolve())
      );
    });
  };

  this.parseCSV = csvData => {
    return new Promise((resolve, reject) => {
      csv.parse(csvData, (err, data) => {
        if (err) return reject(err);
        return resolve(data);
      });
    });
  };

  this.readFile = () => {
    return new Promise((resolve, reject) => {
      if (!this.FILE_PATH) return reject('FILE_PATH is not set');
      fs.readFile(this.FILE_PATH, (err, csvImportData) => {
        return err ? reject(err) : resolve(csvImportData);
      });
    });
  };

  /**
	* Returns an array with columns positions
	* @param {array} columns
	*/
  this.getColumnsIndex = columns => {
    let columnsIdxs = {};
    columns.forEach((column, idx) => (columnsIdxs[column] = parseInt(idx)));
    return columnsIdxs;
  };
};

module.exports = parser;
