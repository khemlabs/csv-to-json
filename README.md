# CSV TO JSON

### FILES

#### CSV

The csv file must exist at *data/* with the name *data.csv*

#### OBJECT STRUCTURE MATCH

The csv will be generated with the structure at *data/match.json*

### EXAMPLES

Examples can be found at *example/*

#### data.csv example

```csv
first_name,last_name,child_unique,child_name,child_last_name,child_age,toy_name
Richard,Parker,"child_1",Peter,Parker,"12",spider
Thomas,Wayne,"child_2",Bruce,Wayne,"15",bat
Thomas,Wayne,"child_2",Bruce,Wayne,"15",car
Bob,Parr,"child_3",Violet,Parr,"15",sword
Bob,Parr,"child_4",Dash,Parr,"10",sword
Bob,Parr,"child_4",Dash,Parr,"10",ball
Bob,Parr,"child_5",Jack,Parr,"2",Baby toy
```

#### match.json example

__THIS KEYS MUST EXISTS:__

* REQUIRED_COLUMNS
* PARENT_ENTITY
* COLUMN_MATCHING IS REQUIRED

```json
{
  "REQUIRED_COLUMNS": ["first_name", "last_name", "child_name", "child_last_name", "child_age", "toy_name"],
  "PARENT_ENTITY": "parents",
  "COLUMN_MATCHING": [
    {
      "entity": "parents",
      "child": "childs",
      "columns": [{ "column": "last_name", "primary": true }, { "column": "first_name" }]
    },
    {
      "entity": "childs",
      "child": "toys",
      "columns": [
        { "column": "child_unique", "jsonKey": "id", "primary": true },
        { "column": "child_last_name", "jsonKey": "last_name" },
        { "column": "child_name", "jsonKey": "first_name" }
      ]
    },
    {
      "entity": "toys",
      "columns": [{ "column": "toy_name", "jsonKey": "toy", "primary": true }]
    }
  ]
}
```

### Who do I talk to?

* dnangelus repo owner and admin
* developer elgambet and khemlabs